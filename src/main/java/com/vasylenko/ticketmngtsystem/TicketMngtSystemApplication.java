package com.vasylenko.ticketmngtsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketMngtSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketMngtSystemApplication.class, args);
	}

}
